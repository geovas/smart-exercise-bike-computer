# README #

### What is it? ###

* Smart Exercise Bike Computer Firmware
* FW Version: v1.01
* Development board: FRDM-K64F
* Wireless interfaces: NO
* Author: Yuri Tikhonov (Russia)

### What your need to start this application? ###

* IDE: Keil MDK v5.24
* Legacy packets: NO
* Project file: bike_k64f.uvprojx
* MCU: NXP MK64FN1M0VLL12
* Clock friquency: 120 MHz

### Roadmap ###

* ~~Initial commit~~
* ~~Add distance and speed measurements~~
* ~~Add calories measurements~~
* ~~Add riding time calculation~~
* ~~Add smart paramenters measurements~~
* ~~Add LCD drivers~~
* ~~Add ADC driver for tension measurements~~
* ~~Make a bridge with FRDM-KW41Z~~
* Add new capabilities from FRDM-KW41Z branch
