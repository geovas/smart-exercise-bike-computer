// =============================================
//      LCD-library of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

#ifndef __LCD_H
#define __LCD_H

// ---------------------------------------------
//                Definitions
// ---------------------------------------------

// General LCD 44780 settings
#define LCD_I2C						(I2C0)
#define LCD_BACKLIGHT			(1) 		// backlight can be ON = '1' or OFF = '0'
#define LCD_ADDR					(0x20)  // maybe 0x40 !!!
#define LCD_DATA					(0x01)
#define LCD_COMM					(0x00)


// Text positions on LCD screen
#define TEXT_UP_LEFT 			1
#define TEXT_UP_RIGHT 		2
#define TEXT_UP_CENTER 		4
#define TEXT_DOWN_LEFT 		10
#define TEXT_DOWN_RIGHT		20
#define TEXT_DOWN_CENTER	40

// ---------------------------------------------
//             Class declaration
// ---------------------------------------------

class lcd
{
	private:
		static void send_two_bytes(char a, char b);			// send two bytes through I2C
	
	public:
		static void init(void);													// initiate LCD
		static void write(char pos, const char *text); 	// put text on LCD
		static void write(char pos, float value);				// put value on LCD
};

// ---------------------------------------------

#endif
