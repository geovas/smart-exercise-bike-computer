// =============================================
//      GPIO-library of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

// ---------------------------------------------
//               Include libraries
// ---------------------------------------------

#include "board.h"						// FRDM-K64F specific library
#include "fsl_gpio.h"					// SDK GPIO library
#include "pin_mux.h"					// GUI configurable "pin map"

#include "gpio.h"							// user defined GPIO library

// ---------------------------------------------
//               Class functions
// ---------------------------------------------

void gpio::init(void) // initiate GPIO lines
{
	// basic board settings
	BOARD_InitPins();
	
	// led is an output
	gpio_pin_config_t led;
	led.pinDirection = kGPIO_DigitalOutput;
	led.outputLogic = 0;
	
	// key is an input
	gpio_pin_config_t key;
	key.pinDirection = kGPIO_DigitalInput;
	
	// config LED pins
	GPIO_PinInit(GPIO_RED_NAME, 	GPIO_RED_PIN,		&led);
	GPIO_PinInit(GPIO_GREEN_NAME, GPIO_GREEN_PIN,	&led);
	GPIO_PinInit(GPIO_BLUE_NAME,	GPIO_BLUE_PIN,	&led);
	
	// turn off LED pins
	GPIO_PortSet(GPIO_RED_NAME,   1u << GPIO_RED_PIN);
	GPIO_PortSet(GPIO_GREEN_NAME, 1u << GPIO_GREEN_PIN);
	GPIO_PortSet(GPIO_BLUE_NAME,  1u << GPIO_BLUE_PIN);

	// config KEY pins
	GPIO_PinInit(GPIO_SW2_NAME,		GPIO_SW2_PIN,	 &key);
	GPIO_PinInit(GPIO_SW3_NAME,		GPIO_SW3_PIN,	 &key);
	GPIO_PinInit(GPIO_REED_NAME,	GPIO_REED_PIN, &key);
}

void gpio::write(char pin, char state) // set line 'pin' to condition 'state'
{
	GPIO_Type			*led_name;
	volatile unsigned int	 led_pin;
	
	switch (pin)
	{
		case 0:  led_name = GPIO_RED_NAME;	 	led_pin = GPIO_RED_PIN;		break;
		case 1:  led_name = GPIO_GREEN_NAME; 	led_pin = GPIO_GREEN_PIN; break;
		case 2:  led_name = GPIO_BLUE_NAME;		led_pin = GPIO_BLUE_PIN;	break;
		default: led_name = GPIO_RED_NAME;		led_pin = GPIO_RED_PIN;		break;
	}
	
	if (!state) GPIO_PortSet( led_name, 1u << led_pin );
	else			  GPIO_PortClear( led_name, 1u << led_pin );
	
	return;
}

char gpio::read(char pin)	// get state of line 'pin'
{
	unsigned int state = 0;
	
	switch (pin)
	{
		case 2:  state = GPIO_PinRead(GPIO_SW2_NAME,  GPIO_SW2_PIN);	break;
		case 3:  state = GPIO_PinRead(GPIO_SW3_NAME,  GPIO_SW3_PIN);	break;
		case 4:  state = GPIO_PinRead(GPIO_REED_NAME, GPIO_REED_PIN);	break;
	}

	return (char)(state == 0);
}

// ---------------------------------------------

