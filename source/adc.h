// =============================================
//      ADC-library of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

#ifndef __ADC_H
#define __ADC_H

// ---------------------------------------------
//                Definitions
// ---------------------------------------------

#define ADC_TENSION			0							// ADC line connected to "tension" potentiometer
#define ADC_TENSION_GR	0u						// Group of "tension" line
#define ADC_TENSION_CH	1u						// Channel of "tension" line
#define ADC_BASE				ADC0					// ADC module that we use with "tension" line

#define ADC_VREF				3300					// reference voltage (in mV)



// ---------------------------------------------
//             Class declaration
// ---------------------------------------------

class adc
{
	public:
		static void init(void);				// initiate ADC
		static float read(char pin);	// get analog-data from line 'pin'
};

// ---------------------------------------------

#endif
