// =============================================
//      GPIO-library of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

#ifndef __GPIO_H
#define __GPIO_H

// ---------------------------------------------
//                Definitions
// ---------------------------------------------

#define RED							0
#define GPIO_RED_NAME		BOARD_LED_RED_GPIO
#define GPIO_RED_PIN		BOARD_LED_RED_GPIO_PIN

#define GREEN						1
#define GPIO_GREEN_NAME BOARD_LED_GREEN_GPIO
#define GPIO_GREEN_PIN  BOARD_LED_GREEN_GPIO_PIN

#define BLUE						2
#define GPIO_BLUE_NAME	BOARD_LED_BLUE_GPIO
#define GPIO_BLUE_PIN		BOARD_LED_BLUE_GPIO_PIN


#define SW2							2
#define GPIO_SW2_NAME		GPIOC
#define GPIO_SW2_PIN		6

#define SW3							3
#define GPIO_SW3_NAME		GPIOA
#define GPIO_SW3_PIN		4

#define REED						4
#define GPIO_REED_NAME	GPIOC
#define GPIO_REED_PIN		10


// ---------------------------------------------
//             Class declaration
// ---------------------------------------------

class gpio
{
	public:
		static void init(void);										// initiate GPIO lines
		static void write(char pin, char state);	// set line 'pin' to condition 'state'
		static char read(char pin);								// get state of line 'pin'
};

// ---------------------------------------------

#endif
