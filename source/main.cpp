// =============================================
//      MAIN-file of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

// ---------------------------------------------
//               Include libraries
// ---------------------------------------------

#include "board.h"							// FRDM-K64F specific library
#include "fsl_gpio.h"						// SDK GPIO library
#include "fsl_uart.h"						// SDK UART library
#include "clock_config.h"				// GUI configurable "system clock"
#include "pin_mux.h"						// GUI configurable "pin map"

#include <stdio.h>							// standard IO library
#include <math.h>								// mathematical library
#include <cmsis_os.h>						// work with RTOS Keil RTX

#include "gpio.h"								// user defined GPIO library
#include "adc.h"								// user defined ADC library
#include "lcd.h"								// user defined LCD library


// ---------------------------------------------
//     Global variables and definitions
// ---------------------------------------------

#define TENSION_MODE			0			// if '0' then we set a gear to '8' permanently
																// if '1' then we set a gear by buttons on keyboard
																// if '2' then we set a gear by potentiometer

#define EXPERIMENT				0			// if '0' it's not an experiment
																// if '1' it's an experiment with LCD #1
																// if '2' it's an experiment with LCD #2

char str[100];									// string for for different needs

volatile float speed 		= 0.0f;	// speed of the exercise bike [km/t]
volatile float turns    = 0.0f; // speed in turns per minuter [RPM]
volatile long long tcnt = 0;		// total conunt of turns
volatile float gear	    = 8.0f;	// bicycle gear
volatile float distance = 0.0f;	// distance of the trip [km]
volatile float calories = 0.0f;	// energy in calories [kcal]
volatile float time 		= 0;		// time in seconds

volatile char left      = 0;		// left screen paramenter
volatile char right			= 5;		// right screen parameter

// ---------------------------------------------
//              Service functions
// ---------------------------------------------

// get the line of "smart" function
const char *smart_line(int i)
{
	if (i > +5) i = +5;
	if (i < -5) i = -5;
	
	switch (i)
	{
		case -5:	return " -----  ";
		case -4:	return " ----   ";
		case -3:	return " ---    ";
		case -2:	return " --     ";
		case -1:	return " -      ";
		case 0: 	return " OK     ";
		case 1: 	return " +      ";
		case 2: 	return " ++     ";
		case 3: 	return " +++    ";
		case 4: 	return " ++++   ";
		case 5: 	return " +++++  ";
	}
	return "ERROR   ";	
}

// get the name of mode
const char *mode_name(char i)
{
	switch (i)
	{
		case 0:	return " Speed  ";
		case 1:	return " Turns  ";
		case 2:	return " Gear   ";
		case 3:	return " Dist.  ";
		case 4:	return " Energy ";
		case 5:	return " Time   ";
		case 6:	return " AST 1  ";
		case 7:	return " AST 2  ";
		case 8:	return " AST 3  ";
	}
	return "ERROR   ";
}

// get the value of the parameter in 'mode'
const char *mode_value(char i)
{
	int min, sec, m;
	float smart;
	
	switch(i)
	{
		case 0:	sprintf(str, " %02.2f    ", speed); break;
		case 1:	sprintf(str, " %03.1f    ", turns); break;
		case 2:	sprintf(str, " %1.2f    ", gear); break;
		case 3:	sprintf(str, " %2.2f    ", distance); break;
		case 4:	sprintf(str, " %3.1f    ", calories); break;
		
		case 5:
			min = (long long)time / 60;
			sec = (long long)time - min * 60;
			sprintf(str, " %03d:%02d ", min, sec);
		break;
		
		case 6: // smart mode #1
			smart = (53.33f - distance) / (6e3f - time) * 3600;
		
			memcpy(str, smart_line(smart - speed), 8);
		break;
		
		case 7: // smart mode #2
			m = ((long long)time / 20) % 2;
			if (m==0) smart = (53.33f - distance) / (6e3f - time) * 3600 * 0.75f;
			if (m==1) smart = (53.33f - distance) / (6e3f - time) * 3600 * 1.25f;
		
			memcpy(str, smart_line(smart - speed), 8);
		break;
		
		case 8: // smart mode #3
			m = ((long long)time / 1200) % 5;
			if (m==0) 		smart = (8.000f - distance) / (1.2e3f - time) * 3600 * 0.75f;
			else if (m<4) smart = (40.00f - distance) / (4.8e3f - time) * 3600 * 1.00f;
			else					smart = (53.33f - distance) / (6.0e3f - time) * 3600 * 1.25f;
			
			memcpy(str, smart_line(smart - speed), 8);
		break;
	}
	
	return str;
}

// convert from 'button' to gear
float button_to_gear(float value)
{
	if (gpio::read(SW2))
	{
		value += 1.0f;
		while (gpio::read(SW2)) __ASM("NOP");
	}
	if (gpio::read(SW3))
	{
		value -= 1.0f;
		while (gpio::read(SW3)) __ASM("NOP");
	}
	
	if (value < 1) value = 1.0f;
	if (value > 8) value = 8.0f;
	
	return value;
}

// convert from ADC to gear
float adc_to_gear(void)
{
	float value = adc::read(ADC_TENSION);
	
	value = (int)(value*10.0f+0.5f) / 10.0f;
	
	value = (value - 1.0f) * 6.0f;
	if (value < 1) value = 1.0f;
	if (value > 8) value = 8.0f;
	
	return value;
}

// convert from gear to tension
float gear_to_tension(char gear)
{
	float g   = (float)gear;
	float ans = 5.49f - 0.26f*g + 0.13f*g*g;
	
	return ans;
}


// convert from rotation count to distance in [km]
float rotation_to_distance(unsigned int count, char gear)
{
	float ans = gear_to_tension(gear) / gear_to_tension(8);
	ans = ans * count * 6.67f / 1000.0f;
	
	return ans;
}


// convert from wavelength [ms] to speed in [km/h]
float wavelength_to_speed(unsigned int time, char gear)
{
	float w = 60000.0f / (float)time;
	float ans = gear_to_tension(gear) / gear_to_tension(8);
	ans = ans * 32.0f * w / 80.0f;
	
	return ans;
}


// ---------------------------------------------
//                RTOS threads
// ---------------------------------------------

// first thread: for work with indication
static void indication(void const *args);
osThreadDef(indication, osPriorityNormal, 1, 0);
static void indication(void const *args)
{
	static int counts = 0;	// counter for LCD refrashing
	
	for (;;)
	{
		// refrash LCD data
		counts++;
		if (counts > 10)
		{
			// output data to LCD
			lcd::write(TEXT_UP_LEFT,		mode_name(left));
			lcd::write(TEXT_UP_RIGHT,		mode_name(right));
			lcd::write(TEXT_DOWN_LEFT,  mode_value(left));
			lcd::write(TEXT_DOWN_RIGHT, mode_value(right));
			counts = 0;
			
			gpio::write(RED, true);
			//unsigned char value = tcnt % 0xff;
			//UART_WriteBlocking(UART3, &value, 1);
			osDelay(25);
			gpio::write(RED, false);
			//UART_ReadBlocking(UART3, &value, 1);
			__ASM("NOP");
		}
	}
}


// ---------------------------------------------

// second thread: for work with sensors
static void sensors(void const *args);
osThreadDef(sensors, osPriorityNormal, 1, 0);
static void sensors(void const *args)
{
	static char tcnt_flag   = 0;	// flag for Bluetooth feature
	
	char 				 state			= 0;	// current state of reed
	unsigned int count			= 0;	// count of rotations
	unsigned int start_time = 0;	// start time for speed calculation
	unsigned int stop_time	= 0;	// stop time for speed calculation
	float				 clc_speed  = 0;	// calculated speed
	
	// calculation of the distance (rotation count) and wavelength
	for (;;)
	{
		if (!state && gpio::read(REED))
		{
			stop_time = osKernelSysTick() - start_time;
			stop_time = stop_time / osKernelSysTickMicroSec(1000);
			
			if (stop_time < 4000)	time += stop_time / 1000.0f;
			tcnt_flag = 1;
			
			osDelay(10);
			state = 1;
			count++;
		}
		else if (state && !gpio::read(REED))
		{
			start_time = osKernelSysTick();
			
			osDelay(10);
			state = 0;
		}

		// getting "gear" value
		#if (TENSION_MODE == 0)
			gear = 8;
		#elif (TENSION_MODE == 1)
			gear = button_to_gear(gear);
		#elif (TENSION_MODE == 2)
			gear = adc_to_gear();
		#endif
		
		// convert rotation count to distance [km]
		distance = rotation_to_distance(count, gear);
		
		// convert wavelength [ms] to speed [km/h]
		clc_speed = wavelength_to_speed(stop_time, gear);
		if (clc_speed >= 5.0f && clc_speed <= 240.0f)
			speed = wavelength_to_speed(stop_time, gear);
		else
			speed = 0;

		if (tcnt_flag == 1)
		{
			tcnt += clc_speed * 20.5f / 60.0f * 5.0f;
			tcnt_flag = 0;
		}

		
		// optionaly: convert wavelength [ms] to RMP (turns)
		turns = 6e4f / stop_time;
		
		// optionaly: convert tension [Nm] and turns [RPM] to power [kkcal/h]
		// calories = gear_to_tension(gear) * turns / 9.549f;
		
		// optionaly: convert distance to energy [kkcal]
		calories = 479.97f / 32.0f * distance;
		
	}
}

// ---------------------------------------------

// third thread: for work with keyboard
static void keyboard(void const *args);
osThreadDef(keyboard, osPriorityNormal, 1, 0);
static void keyboard(void const *args)
{
	for(;;)
	{
		// switch bitween screens
		if (gpio::read(SW2) == 1)
		{
			gpio::write(RED, 1);
			osDelay(10);
			while (gpio::read(SW2) == 1) __ASM("NOP");
			gpio::write(RED, 0);
			left++;
			if (left > 8) left = 0;
		}
		if (gpio::read(SW3) == 1)
		{
			gpio::write(GREEN, 1);
			osDelay(10);
			while (gpio::read(SW3) == 1) __ASM("NOP");
			gpio::write(GREEN, 0);
			right++;
			if (right > 8) right = 0;
		}
		
	}
}


// ---------------------------------------------

// forth thread: for work with interconnection
static void interconnect(void const *args);
osThreadDef(interconnect, osPriorityNormal, 1, 0);
static void interconnect(void const *args)
{
	for(;;)
	{
		unsigned char value;
		UART_ReadBlocking(UART3, &value, 1);
		value = tcnt % 0xff;
		UART_WriteBlocking(UART3, &value, 1);
	}
}


// ---------------------------------------------
//               Main function
// ---------------------------------------------

int main(void)
{
	// system init
	gpio::init();
	#if (TENSION_MODE == 2)
		adc::init();
	#endif
	BOARD_BootClockRUN();
	
	// init of the UART
	uart_config_t config;
	UART_GetDefaultConfig(&config);
	config.baudRate_Bps = 19200;
	config.enableRx = true;
	config.enableTx = true;
	
	long frq = CLOCK_GetFreq(kCLOCK_CoreSysClk);
	UART_Init(UART3, &config, frq);
	
	
	lcd::init();
	lcd::write(TEXT_UP_LEFT,  "Speed");
	lcd::write(TEXT_UP_RIGHT, "Distance");

	// first experiment
	#if (EXPERIMENT == 1)
		lcd::write(TEXT_DOWN_CENTER, "Hello Element14!");
		//lcd::write(TEXT_DOWN_LEFT, 100.0f);
		//lcd::write(TEXT_DOWN_RIGHT, 50.0f);
		return 0;
	#endif
	
	// second experiment
	#if (EXPERIMENT == 2)
		for(;;)
		{
			osDelay(10);
			lcd::write(TEXT_DOWN_RIGHT, 49.0f);
			lcd::write(TEXT_DOWN_LEFT, 199.0f);
		
			osDelay(10);
			lcd::write(TEXT_DOWN_RIGHT, 50.0f);
			lcd::write(TEXT_DOWN_LEFT, 100.0f);
		
		}
	#endif

	// if it's not an experiment then - start RTOS threads
	osThreadCreate (osThread(sensors), NULL);
	osThreadCreate (osThread(indication), NULL);
	osThreadCreate (osThread(keyboard), NULL);
	osThreadCreate (osThread(interconnect), NULL);
	
	// empty infinity loop
  for(;;)
	{
		osSignalWait(0, osWaitForever);
  }
}


// ---------------------------------------------
