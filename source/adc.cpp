// =============================================
//      ADC-library of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

// ---------------------------------------------
//               Include libraries
// ---------------------------------------------

#include "board.h"						// FRDM-K64F specific library
#include "fsl_adc16.h"				// SDK GPIO library
#include "pin_mux.h"					// GUI configurable "pin map"

#include "adc.h"							// user defined GPIO library

// ---------------------------------------------
//               Class functions
// ---------------------------------------------

void adc::init(void) // initiate ADC
{
	// configure ADC
	adc16_config_t hadc;
	ADC16_GetDefaultConfig(&hadc);
	
	#if (ADC_VREF < 2000)
		// if defeined, then: VREF = 1.2v
		hadc.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
	#endif
	ADC16_Init(ADC_BASE, &hadc);
	ADC16_EnableHardwareTrigger(ADC_BASE, false);

	return;
}


float adc::read(char pin)	// get analog-data from line 'pin'
{
	// invalid data
	if (pin != ADC_TENSION) return -1.0f;
	
	// configure Line and start conversion
	adc16_channel_config_t hadc_ch;	
	hadc_ch.channelNumber = ADC_TENSION_CH;
	hadc_ch.enableInterruptOnConversionCompleted = false;
	hadc_ch.enableDifferentialConversion = false;
	ADC16_SetChannelConfig(ADC_BASE, ADC_TENSION_GR, &hadc_ch);
	
	// wait a new analog value
	while (!ADC16_GetChannelStatusFlags(ADC_BASE, ADC_TENSION_GR) & kADC16_ChannelConversionDoneFlag)
	{
		__ASM("NOP");
	}
	
	// return measured analog value (in volts)
	float value = ADC16_GetChannelConversionValue(ADC_BASE, ADC_TENSION_GR);
	value = value * (float)ADC_VREF / 4.96e6f;
	return value;
}

// ---------------------------------------------

