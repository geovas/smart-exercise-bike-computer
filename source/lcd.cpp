// =============================================
//      LCD-library of ExerciseBike project
//      Author: Tikhonov Yuri
//      Version: v1.00 (alpha)
// =============================================

// ---------------------------------------------
//               Include libraries
// ---------------------------------------------

#include <cmsis_os.h>					// work with RTOS Keil RTX
#include <stdio.h>						// standard IO library

#include "board.h"						// FRDM-K64F specific library
#include "fsl_i2c.h"					// SDK I2C library
#include "pin_mux.h"					// GUI configurable "pin map"

#include "lcd.h"							// user defined LCD library

// ---------------------------------------------
//               Class functions
// ---------------------------------------------

void lcd::init(void) // initiate GPIO lines
{
	i2c_master_config_t master;
	I2C_MasterGetDefaultConfig(&master);
	I2C_MasterInit(LCD_I2C, &master, 120e6f);
	
	send_two_bytes(0x30, LCD_COMM); osDelay(5);
	send_two_bytes(0x30, LCD_COMM); osDelay(5);
	send_two_bytes(0x30, LCD_COMM); osDelay(5);
	send_two_bytes(0x38, LCD_COMM); osDelay(5);
	send_two_bytes(0x08, LCD_COMM); osDelay(5);
	send_two_bytes(0x01, LCD_COMM); osDelay(5);
	send_two_bytes(0x06, LCD_COMM); osDelay(5);
	send_two_bytes(0x0C, LCD_COMM); osDelay(5);

	return;
}

void lcd::send_two_bytes(char a, char b)	// send two bytes through I2C
{
	unsigned char cmd_1 = ( 1<<0 | 0<<1 | b<<2 | LCD_BACKLIGHT<<3 );
	unsigned char cmd_2 = ( 0<<0 | 0<<1 | b<<2 | LCD_BACKLIGHT<<3 );
	uint8_t value = a;
	
	i2c_master_transfer_t master;
	memset(&master, 0, sizeof(master));

	master.slaveAddress = LCD_ADDR;
	master.direction = kI2C_Write;
	master.subaddress = cmd_1;
	master.subaddressSize = 1;
	master.data = &value;
	master.dataSize = 1;
	master.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferBlocking(LCD_I2C, &master);
	
	master.subaddress = cmd_2;
	master.subaddressSize = 1;
	I2C_MasterTransferBlocking(LCD_I2C, &master);
	
	return;
}

void lcd::write(char pos, const char *text) // put text on LCD
{
	char len = strlen(text);
	send_two_bytes(0x02, LCD_COMM); // carrier return
	osDelay(5);
	
	switch(pos)
	{
		case TEXT_UP_LEFT:
			if (len > 8) len = 8;
		break;
			
		case TEXT_UP_CENTER:
			if (len > 16) len = 16;
		break;
		
		case TEXT_UP_RIGHT:
			if (len > 8) len = 8;
			for (char i=0; i<8; i++) send_two_bytes(0x14, LCD_COMM); // move carrier right
		break;
		
		case TEXT_DOWN_LEFT:
			if (len > 8) len = 8;
			for (char i=0; i<40; i++) send_two_bytes(0x14, LCD_COMM);
		break;
			
		case TEXT_DOWN_CENTER:
			if (len > 16) len = 16;
			for (char i=0; i<40; i++) send_two_bytes(0x14, LCD_COMM);
		break;
		
		case TEXT_DOWN_RIGHT:
			if (len > 8) len = 8;
			for (char i=0; i<48; i++) send_two_bytes(0x14, LCD_COMM);
		break;	

		default:
			if (len > 32) len = 32;
	}
	
	osDelay(5);
	for (char i=0; i<len; i++)
	{
		send_two_bytes(text[i], LCD_DATA);	// send text to lcd
		osDelay(1);
	}
	
	return;
}

void lcd::write(char pos, float value) // put value on LCD
{
	char text[36];
	sprintf(text, "%.2f", value);
	write(pos, text);
	return;
}

// ---------------------------------------------
